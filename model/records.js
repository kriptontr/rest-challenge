/**
 * exports a Promise that resolves a model object with functions like .queryAll
 * @type {ret}
 */
const db = require("../lib/db");
const validateQuery = require("../schemas/recordQuery.js");
const records = db()
    .then(db => {
        const records = db.collection("records");
        var ret = {};
        /**
         * @param startDate (string like "YYYY-MM-DD" eg(2016-10-26))
         * @param endDate (string like "YYYY-MM-DD" eg(2016-10-26))
         * @param minCount (int)
         * @param maxCount (int)
         * @returns {Promise} that resolves db entries
         */
        ret.queryAll = async (startDate, endDate, minCount, maxCount) => {
            await validateQuery({startDate, endDate, minCount, maxCount});
            return records.aggregate([
                {
                    $match:
                        {
                            $and: [
                                {createdAt: {$gt: new Date(startDate)}},
                                {createdAt: {$lt: new Date(endDate)}}
                            ]
                        }
                },
                {
                    $project: {
                        totalCount: {$sum: "$counts"},
                        createdAt: "$createdAt",
                        key: "$key"
                    }
                },
                {
                    $match:
                        {
                            $and: [
                                {totalCount: {$gt: minCount}},
                                {totalCount: {$lt: maxCount}}
                            ]
                        }
                },
            ]).toArray()
        };
        return ret;
    });
module.exports = records;