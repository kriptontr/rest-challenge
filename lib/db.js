const MongoClient = require('mongodb').MongoClient;
const client = new MongoClient(process.env.CONNSTR, {useNewUrlParser: true});
var _db = {};
var ret = function (dbName = (process.env.DBNAME || "testdb")) {
    if (!_db[dbName]) {
        return client.connect().then(() => {
            _db[dbName] = client.db(dbName);
            _db[dbName].client = client;
            return _db[dbName];
        })
    } else {
        return (Promise.resolve(_db[dbName]))
    }
};

module.exports = ret ;
