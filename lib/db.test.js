describe("dblib module",()=>{
    test(" exports a function",()=>{
        expect(typeof (require("./db"))).toEqual("function")
    });
    test("should throw with no CONNSTR env variable", async (done) => {
        await expect(require("./db")()).rejects.toThrow();
        done();
    })

});
