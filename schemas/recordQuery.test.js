const validateQuery = require("./recordQuery");

describe("recordQuery", () => {
    test("ranges should make sense", async (done) => {
        await expect(validateQuery({
            startDate: "2117-01-01",
            endDate: "2000-01-01",
            maxCount: 1,
            minCount: -1
        })).rejects.toThrow();
        await expect(validateQuery({
            startDate: "2000-01-01",
            endDate: "2001-01-01",
            maxCount: 1,
            minCount: 100
        })).rejects.toThrow();
        done()
    })
});