let Joi = require('@hapi/joi');
Joi = Joi.extend((Joi) => ({
    base: Joi.string(),
    name: "string",
    language: {
        less: "must  be less then {{asd}}",
        greater: "must  be greater then {{asd}}"
    },
    rules: [{
        name: 'less',
        params: {
            q: Joi.alternatives([Joi.string().required(), Joi.func().ref()])
        },
        validate(params, value, state, options) {

            if (new Date(value) >= new Date(state.parent[params.q.root])) {
                // Generate an error, state and options need to be passed
                return this.createError('string.less', {v: value, asd: params.q.root}, state, options);
            }
            return value; // Everything is OK
        }
    },
        {
            name: 'greater',
            params: {
                q: Joi.alternatives([Joi.string().required(), Joi.func().ref()])
            },
            validate(params, value, state, options) {
                if (new Date(value) <= new Date(state.parent[params.q.root])) {
                    // Generate an error, state and options need to be passed
                    return this.createError('string.greater', {v: value, asd: params.q.root}, state, options);
                }
                return value; // Everything is OK
            }
        }]
}));
const schema = Joi.object().keys({
    startDate: Joi.string().regex(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/).less(Joi.ref('endDate')).required(),
    endDate: Joi.string().regex(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/).greater(Joi.ref('startDate')).required(),
    minCount: Joi.number().less(Joi.ref('maxCount')).required(),
    maxCount: Joi.number().greater(Joi.ref('minCount')).required()
});
const ret = function (object) {
    return (new Promise((resolve, reject) => {
        Joi.validate(object, schema, function (err, value) {
            if (err) {
                reject(err)
            }
            resolve(value);
        })
    }))
};
module.exports = ret;