const request = require('supertest');
describe(("db test "), () => {
    beforeAll(async (done) => {
        process.env.ENV = "dev";
        require('dotenv').config({path: require("path").resolve(process.cwd() + '/env.' + process.env.ENV)});// load environment vars from file
        await require("../lib/db")()
            .then(db => {
                db.collection("records")
                    .insertMany([
                        {
                            "key": "YhwZESHNaSY2gZoi",
                            "value": "RvmEC7bQN316vtOHNu6xpOaqO1D1yS746SZu41MrSdepPPRDAW8GnORDqkI2UyObSJcjypiknQHqSYN6u2OgAft1ENp2ABd5FkP5lMvYb4Vmh0ybbHVOIW8tkG0s90vs6QDydVJf45lX",
                            "createdAt": new Date("2016-11-18T08:49:23.108Z"),
                            "counts": [
                                600,
                                0
                            ]
                        },
                        {
                            "key": "8oDJX4FosoHgkUUz",
                            "value": "YwOMcZ80it1KfB9VOo9Z13y0SmtS4aqgUTIIQ6Hd1PaR1jdOGyMmL1isk6saB4QjR1xE19i8D5wvJrLm3ds4svNTayW7SA2JT9Ff0rPA8zq0FSqPPdH0WtRr0jqAQUWhSYqGyLhwdM5s",
                            "createdAt": new Date("2017-02-07T13:23:38.320Z"),
                            "counts": [
                                600,
                                100,
                                400,
                                500,
                                700,
                                200,
                                300
                            ]
                        },
                        {
                            "key": "uiRJzQoDHlFpyjuT",
                            "value": "pn5mKQ2kjPUk9oodAogZeigZelnZR8QdTnQHKvHuUIOZPFGK1kLIblOXJU00S1qdgRXDrylyyVJcUsg9gnxMJl7Z8NX3NowWfeoCnJMhnaIVuafbbRiHqV7fH6BejBSijgqy3491YfxN",
                            "createdAt": new Date("2016-11-02T16:57:25.768Z"),
                            "counts": [
                                700,
                                900,
                                200,
                                400,
                                800
                            ]
                        }
                    ])
            });
        done();
    });
    test("response with error message and records null ", async (done) => {
        const app = await require("../server");
        const response = await request(app)
            .post("/api/records")
            .send({startDate: "2017-01-01", endDate: "2016-01-02", minCount: 0, maxCount: 50000})
            .expect(200)
            .expect('Content-Type', /json/);

        await expect(response.body.code).toEqual(1);
        done();
    });
    test("should return array with correct struct", async (done) => {
        const app = await require("../server");
        const response = await request(app)
            .post("/api/records")
            .send({startDate: "2001-01-01", endDate: "2017-01-02", minCount: 0, maxCount: 50000})
            .expect(200)
            .expect('Content-Type', /json/);
        await expect(response.body.code).toEqual(0);
        done();
    });
    afterAll(async (done) => {
        await require("../lib/db")()
            .then(db => {
                return db.dropDatabase()
                    .then(() => {
                        return db.client.close()
                    })

            });
        jest.resetModules();
        done()
    });
});
