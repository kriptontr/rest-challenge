describe(("db test "), () => {
    const dbName = "recordModelTest";
    let oldDbEnv;
    beforeAll(async (done) => {
        process.env.ENV = "dev";
        require('dotenv').config({path: require("path").resolve(process.cwd() + '/env.' + process.env.ENV)});// load environment vars from file
        oldDbEnv = process.env.dbNAME;
        process.env.DBNAME = dbName;
        await require("../lib/db")()
            .then(db => {
                db.collection("records")
                    .insertMany([
                        {
                            "key": "YhwZESHNaSY2gZoi",
                            "value": "RvmEC7bQN316vtOHNu6xpOaqO1D1yS746SZu41MrSdepPPRDAW8GnORDqkI2UyObSJcjypiknQHqSYN6u2OgAft1ENp2ABd5FkP5lMvYb4Vmh0ybbHVOIW8tkG0s90vs6QDydVJf45lX",
                            "createdAt": new Date("2016-11-18T08:49:23.108Z"),
                            "counts": [
                                600,
                                0
                            ]
                        },
                        {
                            "key": "8oDJX4FosoHgkUUz",
                            "value": "YwOMcZ80it1KfB9VOo9Z13y0SmtS4aqgUTIIQ6Hd1PaR1jdOGyMmL1isk6saB4QjR1xE19i8D5wvJrLm3ds4svNTayW7SA2JT9Ff0rPA8zq0FSqPPdH0WtRr0jqAQUWhSYqGyLhwdM5s",
                            "createdAt": new Date("2017-02-07T13:23:38.320Z"),
                            "counts": [
                                600,
                                100,
                                400,
                                500,
                                700,
                                200,
                                300
                            ]
                        },
                        {
                            "key": "uiRJzQoDHlFpyjuT",
                            "value": "pn5mKQ2kjPUk9oodAogZeigZelnZR8QdTnQHKvHuUIOZPFGK1kLIblOXJU00S1qdgRXDrylyyVJcUsg9gnxMJl7Z8NX3NowWfeoCnJMhnaIVuafbbRiHqV7fH6BejBSijgqy3491YfxN",
                            "createdAt": new Date("2016-11-02T16:57:25.768Z"),
                            "counts": [
                                700,
                                900,
                                200,
                                400,
                                800
                            ]
                        }
                    ])
            });
        done();
    });
    test("should return array with correct struct", async (done) => {
        const recordsModel = await require("../model/records");
        await expect(recordsModel.queryAll).toBeDefined();
        await expect(recordsModel.queryAll("2016-11-10", "2016-11-19", 0, 9000)
            .then(a => {
                return a.length
            }))
            .resolves.toEqual(1);

        done();
    });
    afterAll(async (done) => {
        await require("../lib/db")()
            .then(db => {
                return db.dropDatabase()
                    .then(() => {
                        return db.client.close()
                    })

            });
        process.env.dbNAME = oldDbEnv;
        jest.resetModules();
        done()
    });
});
