#rest-challenge  
Rest challenge for interview
### Installation 
`git clone  git@gitlab.com:kriptontr/rest-challenge.git`  
`cd rest-challenge`  
`npm install `  

### How To Run 
You should provide some environment variables  to run.To do that, you can create env file to specific environmet.Take a look at development env file => [/env.dev](https://gitlab.com/kriptontr/rest-challenge/blob/master/env.dev)  
To run with this config you should run  
`ENV=dev node index.js`  
In this manner, you can create a production config file named /env.prod and run it with  
`ENV=prod node index.js`  

###Testing  
As usual unit test of each file placed filename.test.js within same directory. Integration tests live under /test folder and runs against dev environment