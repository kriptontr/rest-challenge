describe("index", () => {
    describe("Env", () => {
        beforeAll(() => {
            jest.resetModules();
            process.env.ENV = "dev"
        });
        test("load env varibles from file (dev)", async (done) => {
            await require("./index");
            await expect(process.env.CONNSTR).toBeDefined();
            done();
        });
        afterAll(() => {
            jest.resetModules();
           delete (process.env.ENV)
        })
    });
    describe("Env", () => {
        test("throws an error when ENV not Set ", () => {
            jest.resetModules();
            expect(() => {
                require("./index")
            }).toThrow();
        })
    })
});
