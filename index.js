if (!process.env.ENV) {
    throw new Error("ENV env variable must be declared")
}
require('dotenv').config({path: require("path").resolve(process.cwd() + '/env.' + process.env.ENV)}); // load environment vars from file
const app = require("./server");
const port = process.env.PORT || 3000;
module.exports = new Promise((resolve, reject) => {
    app.listen(port, () => {
        console.log(`listening on port ${port}`);
        resolve()
    });
});
