const router = require("express").Router();
const recordModel = require("../model/records");
router.post("/records", function (req, res) {

    recordModel.then(model => {
        model.queryAll(req.body.startDate, req.body.endDate, req.body.minCount, req.body.maxCount)
            .then(data => {
                res.json({code: 0, message: "Success", records: data})
            })
            .catch(err => {
                res.json({code: 1, msg: err.message, records: null})
            })
    })
});

module.exports = router;